.. py:function:: mindpandas.config.get_benchmark_mode()

    获取当前环境"benchmark"模式的状态。

    返回：
        bool，当前"benchmark"模式是否开启。