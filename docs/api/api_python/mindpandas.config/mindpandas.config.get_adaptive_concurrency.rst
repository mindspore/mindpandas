.. py:function:: mindpandas.config.get_adaptive_concurrency()

    获取是否使用自适应并发模式的标记位。

    返回：
        bool，是否使用自适应并发模式。